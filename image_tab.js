window.onload = init_image_tab

function init_image_tab() {

  tbl = document.getElementById(IMAGE_TAB_TABLE_ID)

  for (i = 0; i < tbl.rows.length; i++)
  {
    trElem = tbl.rows[i]

    // remove weight selects
    removeselect = trElem.getElementsByTagName('select').item(0)
    trElem.removeChild(removeselect.parentNode.parentNode)

     // add onclick event to remove link
    removeLnk = trElem.lastChild.getElementsByTagName('checkbox').item(0) // was 'a'
    removeLnk.onclick = function() {removeRow(this)}
    removeLnk.href = "javascript:void(0)"

     // add cel for up and down links
    tdElem = trElem.insertCell(1)
  }
    UpAndDownLinks()
}

function moveRow(obj, direction)
{
  thisRow = obj.parentNode.parentNode
  thisCell = obj.parentNode.parentNode.cells[0]

  if (direction == 'down')
    newCell = thisRow.nextSibling.cells[0]
  else
    newCell = thisRow.previousSibling.cells[0]

  dupThisCell = thisCell.cloneNode(true)
  dupNewCell = newCell.cloneNode(true)
  thisCell.parentNode.replaceChild(dupNewCell, thisCell)
  newCell.parentNode.replaceChild(dupThisCell, newCell)
}

function removeRow(obj)
{
  obj.parentNode.parentNode.parentNode.removeChild(obj.parentNode.parentNode)
  UpAndDownLinks()
}

function UpAndDownLinks(){

  var tbl = document.getElementById(IMAGE_TAB_TABLE_ID)

  for (i = 0; i < tbl.rows.length; i++)
  {

    trElem = tbl.rows[i]
        newCell = document.createElement("td")

    hiddenInput = document.createElement("input")
    hiddenInput.setAttribute("type", "hidden")
    hiddenInput.setAttribute("value", i)
    hiddenInput.setAttribute("name", "edit[image_tab_weight][]")
    newCell.appendChild(hiddenInput)

    hiddenInput = document.createElement("input")


    if (i > 0) {
      upLink = document.createElement("a")
      upLink.onclick = function() {moveRow(this, "up")}
      upLink.href = "javascript:void(0)"
      upLink.appendChild(document.createTextNode(IMAGE_TAB_UP))
      newCell.appendChild(upLink)
    }

    if (i > 0 && i < tbl.rows.length-1) {
      lineBreak = document.createElement("br")
      newCell.appendChild(lineBreak)
    }

    if (i < tbl.rows.length-1) {
      downLink = document.createElement("a")
      downLink.onclick = function() {moveRow(this, "down")}
      downLink.href = "javascript:void(0)"
      downLink.appendChild(document.createTextNode(IMAGE_TAB_DOWN))
      newCell.appendChild(downLink)
    }
    trElem.replaceChild(newCell, trElem.cells[1])
  }
}

/*******************************************************************
* File    : JSFX_SimpleRollover.js  JavaScript-FX.com
*
* Created : 2001/03/19
*
* Author  : Roy Whittle www.Roy.Whittle.com
*
* Purpose : To create image rollovers.
*   For simple image swaps this code is a little too heavy.
*   However, if you have pre image loads and multiple image
*   swaps then this can work out better by allowing the
*   page to load faster.
*   The best feature is the "post" image load.
*
* History
* Date         Version        Description
*
* 2001-03-18  1.0   Initial version for JavaScript-FX.com
* 2001-09-17  1.1   Made the interface the same as for animated
*         and fading rollovers.
***********************************************************************/
if(!window.JSFX)
  JSFX=new Object();

JSFX.RolloverObjects=new Array();

JSFX.Rollover = function(name, img)
{
  JSFX.RolloverObjects[name]= new Image();
  JSFX.RolloverObjects[name].img_src = img;
  if(!JSFX.Rollover.postLoad)
    JSFX.RolloverObjects[name].src = img;
}
JSFX.Rollover.postLoad = false;
JSFX.Rollover.loadImages = function()
{
  var i;
  for(i in JSFX.RolloverObjects)
  {
    r=JSFX.RolloverObjects[i];
    r.src=r.img_src;
  }
}
JSFX.Rollover.error = function(n)
{
//    alert("JSFX.Rollover - An Error has been detected\n"
//      + "----------------------------------\n"
//      + "You must define a JSFX.Rollover in your document\n"
//      + "JSFX.Rollover(\""+n+"\",\"your_on_img.gif\")\n"
//      + "(check the spelling of your JSFX.Rollovers)");
}
JSFX.findImg = function(n, d)
{
  var img = d.images[n];
  if(!img && d.layers)
    for(var i=0 ; !img && i<d.layers.length ; i++)
      img=JSFX.findImg(n,d.layers[i].document);

  /*** Stop emails because the image was named incorrectly ***/
  if(!img)
  {
//    alert("JSFX.Rollover - An Error has been detected\n"
//      + "----------------------------------\n"
//      + "You must define an image in your document\n"
//      + "<IMG SRC=\"your_image.ext\" NAME=\""+n+"\">\n"
//      + "(check the NAME= attribute of your images)");

    return(new Image());
  }
  return img;
}
JSFX.imgOn = function(imgName, rollName)
{
  if(rollName == null)
    rollName=imgName;

  /*** Stop emails because the rollover was named incorrectly ***/
  if(!JSFX.RolloverObjects[rollName])
  {
    JSFX.Rollover.error(rollName);
    return;
  }
  var img = JSFX.findImg(imgName,document);
  if(img.offSrc==null)
    img.offSrc=img.src;
  img.src=JSFX.RolloverObjects[rollName].img_src;
}
JSFX.imgOff = function(imgName)
{
  var img = JSFX.findImg(imgName,document);
  img.src=img.offSrc;
}

function slideDiv(elementId, vector, max_width, speed) {
  if (!document.getElementById) return false;
  if (!document.getElementById(elementId)) return false;

  var element = document.getElementById(elementId);

//   if (element.sliding) return false;

//   if (element.sliding) clearTimeout(element.sliding);
  if (!element.xpos) element.xpos = 0;

  if (vector < 0) {
    x = Math.max(-max_width, element.xpos + vector);
  }
  else {
    x = Math.min(0, element.xpos + vector);
  }

//  alert('elementID: ' + elementId + ', vector: ' + vector + ', max_width: ' + max_width + ', speed: ' + speed + ', x: ' + x);

  return slideElement(elementId, x, 0, speed);
}

/*

This function changes the 'left' and 'top' properties of an element.
The function takes four arguments.
The first argument is the ID of the element to be moved.
The second argument is the 'left' position that the element should be moved to.
The third argument is the 'top' position that the element should be moved to.
The fourth argument is the increment.
This determines how much the element moves each time.
For instance, sending 2 as the increment will halve the distance travelled by the element each time.
This function invokes itself (with a delay) until the element has reached the desired coordinates.

If you'd like to use this function yourself, go ahead.
Drop me a line to let me know where I can see it in action:
http://adactio.com/contact/

*/

function slideElement(elementId,x,y,inc) {
  if (!document.getElementById) return false;
  if (!document.getElementById(elementId)) return false;

  var element = document.getElementById(elementId);

  if (element.sliding) clearTimeout(element.sliding);

  if (!element.xpos) element.xpos = 0;
  if (!element.ypos) element.ypos = 0;

  if (element.xpos == x && element.ypos == y) return true;

  if (element.xpos > x) {

    var dist = Math.ceil((element.xpos-x)/inc);
    element.xpos = element.xpos - dist;

  }

  if (element.xpos < x) {

    var dist = Math.ceil((x-element.xpos)/inc);
    element.xpos = element.xpos + dist;

  }

  if (element.ypos > y) {

    var dist = Math.ceil((element.ypos-y)/inc);
    element.ypos = element.ypos - dist;

  }

  if (element.ypos < y) {

    var dist = Math.ceil((y-element.ypos)/inc);
    element.ypos = element.ypos + dist;

  }

  element.style.left = element.xpos+'px';
  element.style.top = element.ypos+'px';

  element.sliding = setTimeout('slideElement("'+elementId+'",'+x+','+y+','+inc+')',10);
}

